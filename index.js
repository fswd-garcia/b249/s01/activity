console.log('hello');

let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// what array method can we use to add an item at the end of the array?

students.push("Raf");
console.log(students);

// what array method can we use to add an item at the start of the array?

students.unshift("Pinky");
console.log(students);

students.pop();
console.log(students);


students.shift();
console.log(students);

// what is the difference between splice() and slice method()?
/*  
	splice 	- changes the original array
			- mutator methods
			- remove and adding items from a starting index

	slice 	- Non-Mutator methods
			- copies a portion from starting index and returns a new array from it

	Iterator Methods
			- loop over the items of an array

	forEach()
	map()
	every()



*/

// Mini Activity

let arrNum = [15, 20, 25, 30, 11, 7];

/*
	Using forEach, check if every item in the array is divisible by 5. If they are, log in the console "<num> is divisible by 5" if not, log false.

	Kindly send the screenshot 
*/

arrNum.forEach(myFunction);

function myFunction(item) {

    // console.log(item, "is divisible by 5");
    
  // let result = item % 5 === 0 ? "is divisible by 5" : "false";
  console.log(item, (item % 5 === 0 ? "is divisible by 5" : "false"));
}

arrNum.forEach

// Math Object - allows you to perform mathematical tasks on number 

console.log(Math);
console.log(Math.E); //Euler's number
console.log(Math.PI); //PI
console.log(Math.SQRT2); //square root of 2
console.log(Math.SQRT1_2); //square root of 1/2
console.log(Math.LN2); //natural logarithm of 2
console.log(Math.LN10); //natural logarithm of 10
console.log(Math.LOG2E); //base 2 of logarithm of E
console.log(Math.LOG10E); //base 10 of logarithm of E


//Methods for rounding a number to an integer
console.log(Math.round(Math.PI));
console.log(Math.ceil(Math.PI));
console.log(Math.floor(Math.PI));
console.log(Math.trunc(Math.PI));

// returns a square root of a number
console.log(Math.sqrt(3.14));

// lowest value in a list of arguments
console.log(Math.min(-1, -2, -4, -100, 0, 1, 3, 5, 6));

console.log(Math.max(-1, -2, -4, -100, 0, 1, 3, 5, 6));

// Exponent Operator
const number = 5 ** 2;
console.log(number);

console.log(Math.pow(5, 2));


// 5 to 8 as stretch goals
// sort method number 5

let arrayS01 = ["one", "two", "three", "four", "five"];

// ACTIVITY;
/*
1. [];
2. arrayS01[0];
3. arrayS01[arrayOne.length - 1];
4. arrayS01.indexOf(item);
5. forEach
6. map
7. every
8. some
9. false
10.true
*/

console.log(arrayS01[0]);
console.log(arrayS01[arrayS01.length - 1]);
console.log(arrayS01.indexOf("one"));

// Activity Function Coding
// 1.


function addToEnd(arrayName, item){
	if (typeof(item) === "string") {
		arrayName.push(item);
		console.log(students);
	}
	else console.log("error - can only add strings to an array");
};

addToEnd(students, "Ryan");
addToEnd(students, 045);

function addToStart(arrayName, item){
	if (typeof(item) === "string") {
		students.unshift(item);
		console.log(students);
	}
	else console.log("error - can only add strings to an array");
	
}

addToStart(students, "Tess");
addToStart(students, 033);


function elementChecker(arrayName, item){
	if (arrayName.includes(item)){
		console.log((arrayName.includes(item)));
	}
		else console.log("error - passed in array is empty");
}

elementChecker(students,"Jane");

elementChecker([],"Jane");

function checkAllStringsEnding(arrayName,item){
	let errorMessage;
	if (arrayName.length == 0){
		errorMessage = "error - array must NOT be empty1";
		// console.log("emply1");
	}
	let checkString = arrayName.every(item => typeof item == "string");
	// console.log(checkString);
	if (!checkString) errorMessage = "error - all array elements must be strings";
	
	if (typeof(item) != "string") errorMessage = "error - 2nd argument must be data type string";

	// console.log(item.length);
	if (item.length > 1) errorMessage = "2nd argument must be a single character";

	return errorMessage;
}

checkAllStringsEnding(students,"e");
checkAllStringsEnding([],"e");
checkAllStringsEnding(["Jane", 02],"e");
checkAllStringsEnding(students, "e");
checkAllStringsEnding(students, 4);

const stringLengthSorter = (arrayName) => {
	if(arrayName.some(item => typeof item !== "string")){
	 		return "error = all array elements must be strings"
	 	} else {
	 		arrayName.sort((a, b) => {
	 			return a.length - b.length
	 		})
	 		console.log(arrayName)
	 	}
}



stringLengthSorter(students);
stringLengthSorter([037, "John", 039, "Jane"]);